#!/bin/bash

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd $SCRIPTPATH

export ANDROID_PRODUCT_OUT=$SCRIPTPATH/system-images
export ANDROID_BUILD_TOP=$SCRIPTPATH # this must be set to some existing folder otherwise emulator will search for avds and not the ANDROID_PRODUCT_OUT, the directory is never used i think

./android-emulator/linux-x86_64/emulator -kernel Image -no-snapshot -verbose
