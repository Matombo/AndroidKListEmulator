Thanks to Chris Salls for coding the KList challange for a x86 emulator. This is a port of his work to Android.

# Run Emulator
- install adb/android-tools on your system
- this repo pulls in the Android emulator as a submodule
    - run "git submodule init"
    - run "git submodule update"
- run "./run.sh"
- ui is not working and will hang on the Android logo, you can still connect to the console using "adb shell" (if it fails wait a little bit and try again, after a while the emulator will have opened the adb port)

# Compile Exploit
- install aarch64-linux-gnu-gcc compiler to your system
- build exploit using "`aarch64-linux-gnu-gcc -pthread --static android_klist_emulator_exploit.c -o android_klist_emulator_exploit`"

# Deploy and Execute Exploit
- copy exploit to emulator using "adb push android_klist_emulator_exploit /data/local/tmp"
- open shell on the emulator using "adb shell"
- switch to non root user with "su shell"
- execute exploit using "/data/local/tmp/android_klist_emulator_exploit"
- if exploit doesn't work restart emulator and try again

# Bonus
- klist.c is the source code of the vulnerable driver
- vmlinux is the non stripped version of the Image kernel binary

# Build instructions
## Checkout source
- mkdir AndroidSource && cd Android Source
- checkout using my manifest: "repo init -u https://gitlab.cs.fau.de/Matombo/AndroidManifest.git -b android-7.1.2_r28_klist && repo sync -j8"

## Build Kernel
- download suitable andoird-ndk to your system (the newest ones will not work, i used version 14)
- git clone https://gitlab.cs.fau.de/Matombo/AndroidKernelGoldfish
- cd AndroidKernelGoldfish
- git checkout android-goldfish-3.10_klist
- build and deploy using this tutorial: https://source.android.com/setup/build/building-kernels
- quick list of required commands in order
    - cd AndroidKernelGoldfishKList
    - export ARCH=arm64
    - export CROSS_COMPILE=<path_to_ndk>/toolchains/aarch64-linux-android-4.9/prebuilt/linux-x86_64/bin/aarch64-linux-android-
    - make ranchu_defconfig
    - make -j8

## Build Android image
- install fastboot/android-tools and jdk8 to your system
- build and deploy using this tutorial: https://source.android.com/setup/build/building or by following "Build Android and Kernel.md"
- quick list of required commands in order
    - cd <path_to_android_source>
    - export LC_ALL=C
    - source build/envsetup.sh
    - lunch aosp_arm64-eng
    - make -j8

## Launch Android image and kernel in emulator
- mkdir system-images
- mkdir system-images/system
- cp <path_to_android_source>/target/product/generic_arm64/system.img <path_to_android_source>/target/product/generic_arm64/ramdisk.img <path_to_android_source>/target/product/generic_arm64/userdata.img <path_to_android_source>/target/product/generic_arm64/userdata-qemu.img system-images/
- cp <path_to_android_source>/target/product/generic_arm64/system/build.prop system-images/system/
- cp <path_to_goldfish_kernel>/Klist/AndroidKernel/Default/Image .
- ANDROID_PRODUCT_OUT=$PWD/system-images ANDROID_BUILD_TOP=$PWD <path_to_android_emulator>/android-emulator/linux-x86_64/emulator -kernel Image -no-snapshot -verbose
- ways to get the emulator are the offical android-sdk or the offical prebuilt repository: https://android.googlesource.com/platform/prebuilts/android-emulator/
